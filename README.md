# Arch Linux Basisinstallation
## Ziel

+ Secure Boot
+ Festplatte mit LVM aufteilbar
+ Festplatte verschlüsselt
+ i3 als Window Manager
+ Netzwerkdrucker aktiviert
+ Windows VM installiert
    

## Vorbereitung
```bash
loadkeys de
loadkeys de-latin1
```

## Partitionierung

```bash
cgdisk
```

Partitionen:

1. EFI System boot Partition 1gig -> Name boot  -- sdx1
2. restliche Partition LVM -> Name LVM -- sdx2

## LVM Partition verschlüsseln

Verschlüsseln:
```bash
modprobe dm-crypt
cryptsetup -c aes-xts-plain -y -s 512 luksFormat /dev/sdx2
```

Öffnen:
```bash
cryptsetup luksOpen /dev/sdx2 lvm
```

## LVM anlegen

```bash
pvcreate /dev/mapper/lvm
vgcreate main /dev/mapper/lvm
lvcreate -L 80GB -n root main
lvcreate -L 16GB -n swap main
lvcreate -L 100GB -n home main
```

## Dateisysteme anlegen

```bash
mkfs.ext4 -L root /dev/mapper/main-root
mkfs.ext4 -L home /dev/mapper/main-home
mkfs.fat -F 32 -n EFIBOOT /dev/sdx1
mkswap -L swap /dev/mapper/main-swap
```

## Verzeichnisstruktur für die Installation mounten und swap aktivieren

```bash
mount /dev/mapper/main-root /mnt
mkdir /mnt/home
mount /dev/mapper/main-home /mnt/home
mkdir /mnt/boot
mount /dev/sdx1 /mnt/boot
swapon /dev/mapper/main-swap
```

## Installation des Arch Systems

```bash
pacstrap /mnt base base-devel intel-ucode efibootmgr grub dosfstools vim 
genfstab -Lp /mnt > /mnt/etc/fstab
arch-chroot /mnt
```

Im arch-chroot in der Konmfigurationsdatei /etc/mkinitcpio.conf encrypt ergänzen (hinter base udev und vor lvm2)

```bash
vim /etc/locale.gen # hier die Lokale anpassen utf-8
locale-gen
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "KEYMAP=de-latin1" > /etc/vconsole.conf
echo "arch-desktop" > /etc/hostname # rechnername
```

## Bootloader installieren

```bash
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch_grub --recheck --debug
mkdir  -p /boot/grub/locale
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/grub.cfg
```

## Nachbereitung

### Betriebssystem für SSD konfigurieren
```zsh

```


### zsh als Standardshell einrichten

```zsh

```

## Programme installieren

Arch Official Repo:
+ grml-zsh-config
+ i3, dmenu
+ chromium
+ xorg-xrandr, arandr
+ rxvt-unicode
+ eclipse-java
+ alsa-utils
+ cups, avahi-daemon, nss-mdns, Druckerspezifische Treiber (z.B. hplip) (siehe Arch Wiki, Network Printing)

AUR:
+ vs-code (oder direkt das binary downloaden)
+ google-chrome

Startup konfigurieren:
+ .xinitrc <- xrandr dpi einstellungen und 
+ .Xdefaults <- Farbschema für urxvt
bildschirmeinstellungen

Netzwerkdrucker konfigurieren:
+ /etc/nsswitch.conf


Drucker installieren

